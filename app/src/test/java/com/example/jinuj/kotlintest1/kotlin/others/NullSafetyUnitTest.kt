package com.example.jinuj.kotlintest1.kotlin.others

import org.junit.Test

/**
 * Created by jinu.j on 14-06-2017.
 **/
class NullSafetyUnitTest {
    var b: String? = "abc"

    @Test
    fun CheckingForNull() {
        val b: String? = "abc"
        val l = if (b != null) b else ""
        println(l)
    }

    @Test
    fun SafeCalls() {
        b = null
        println(b?.length)
    }

    @Test
    fun ElvisOperator() {
        b = null
        val l = b?.length ?: -1
        println(l)
    }


    @Test(expected = KotlinNullPointerException::class)
    fun TheExclmOperator() {
        b = null
        val l = b!!.length
        println(l)
    }

    @Test
    fun SafeCasts() {
        b = null
        val aInt: Int? = b as? Int
        println(aInt)
    }


    @Test
    fun CollectionsOfNullableType() {
        val nullableList: List<Int?> = listOf(1, 2, null, 4)
        val intList: List<Int> = nullableList.filterNotNull()
        println(intList)
    }

}