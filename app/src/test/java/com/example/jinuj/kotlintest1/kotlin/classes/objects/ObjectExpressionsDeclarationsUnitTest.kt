package com.example.jinuj.kotlintest1.kotlin.classes.objects

import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by jinu.j on 09-06-2017.
 **/
class ObjectExpressionsDeclarationsUnitTest {

    open class A(x: Int) {
        open val y: Int = x
    }

    interface B

    val ab: A = object : A(1), B {
        override val y = 15
    }

    @Test
    fun objectTest() {
        DataProvider.getRetrofit()
        DefaultListener.mouseClicked("e")
        println(ab)
    }

    object DataProvider {
        private var retrofit: Retrofit? = null
        private val url: String? = "https://raw.githubusercontent.com/jinujayakumar/android-path-animation/"

        fun getRetrofit(): Retrofit? {
            if (retrofit == null) {
                retrofit = Retrofit
                        .Builder()
                        .baseUrl(url)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
            }
            return retrofit
        }
    }

    object DefaultListener : MouseAdapter {
        override fun mouseClicked(e: String) {
            // ...
        }

        override fun mouseEntered(e: String) {
            // ...
        }

    }

    open interface MouseAdapter {
        fun mouseClicked(e: String)

        fun mouseEntered(e: String)

    }


}



