package com.example.jinuj.kotlintest1.kotlin.classes.objects

import android.content.Context
import android.util.AttributeSet
import android.view.View

/**
 * Created by jinu.j on 07-06-2017.
 **/
class ClassesAndInheritanceUnitTest {


    class Invoice

    class Person constructor(firstName: String)

    class Customer(name: String) {
        init {
            println("Customer initialized with value $name")
        }
    }

    abstract class Base {

        var x: String?
            get() {
                return x
            }
            set(value) {
                x = value
            }
        var p: Int?
            get() {
                return p
            }
            set(value) {
                p = value
            }


        constructor(x: String) {
            this.x = x
        }


        constructor(p: Int) {
            this.p = p
        }

        open fun base() {}

        fun nv() {}
    }

    class X : Base {
        constructor(int: Int) : super(int)

        constructor(string: String) : super(string)

        init {
            x = "jinu"
        }

        override fun base() {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

    }

    class MyView : View {

        constructor(context: Context) : super(context)

        constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)
    }


    open class A {
        open fun f() {
            print("A")
        }

        open fun b() {
            print("a")
        }
    }

    interface B {
        fun f() {
            print("B")
        } // interface members are 'open' by default

        fun b() {
            print("b")
        }
    }

    class C() : A(), B {
        // The compiler requires f() to be overridden:
        override fun f() {
            super<A>.f() // call to A.f()
            super<B>.f() // call to B.f()
        }

        override fun b() {
            super<A>.b()
            super<B>.b()
        }
    }
}