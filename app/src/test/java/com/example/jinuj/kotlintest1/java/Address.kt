package com.example.jinuj.kotlintest1.java

/**
 * Created by jinu.j on 07-06-2017.
 **/
data class Address(var name: String? = null,
                   var street: String? = null,
                   var city: String? = null,
                   var state: String? = null,
                   var zip: String? = null)