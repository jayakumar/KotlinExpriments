package com.example.jinuj.kotlintest1.kotlin.classes.objects

import org.junit.Test

/**
 * Created by jinu.j on 07-06-2017.
 **/
class InterfacesUnitTest {

    interface MyInterface {
        fun bar()
        fun foo() {
            println("Hi")
        }
    }

    class Child : MyInterface {
        override fun bar() {
            // body
        }
    }

    interface MyInterface1 {
        val prop: Int // abstract

        val propertyWithImplementation: String
            get() = "foo"

        fun foo() {
            print(prop)
        }
    }

    class Child1 : MyInterface1 {
        override val prop: Int = 29
    }

    interface A {
        fun foo() { print("A") }
        fun bar()
    }

    interface B {
        fun foo() { print("B") }
        fun bar() { print("bar") }
    }

    class C : A {
        override fun bar() { print("bar") }
    }

    class D : A, B {
        override fun foo() {
            super<A>.foo()
            super<B>.foo()
        }

        override fun bar() {
            super<B>.bar()
        }
    }

    @Test
    fun checkInterface() {
        Child().foo()
        val child = Child1()
        child.foo()

    }

}