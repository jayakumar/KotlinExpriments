package com.example.jinuj.kotlintest1.kotlin.classes.objects

import org.junit.Test

/**
 * Created by jinu.j on 12-06-2017.
 **/
class DelegationUnitTest {


    interface Nameable {
        var name: String
    }

    class JackName : Nameable {
        override var name: String = "Jack"
    }

    class LongDistanceRunner : Runnable {
        override fun run() {
            println("long")
        }
    }

    class Person(name: Nameable, runner: Runnable) : Nameable by name, Runnable by runner

    interface Base {
        fun print()
    }

    class BaseImpl(val x: Int) : Base {
        override fun print() {
            print(x)
        }
    }

    val lazyValue: String by lazy {
        println("computed!")
        "Hello"
    }

    class Derived(b: Base) : Base by b

    @Test
    fun testDelegate() {
        val b = BaseImpl(10)
        Derived(b).print() // prints 10
    }

    @Test
    fun testDeligete2() {
        val y = Person(JackName(), LongDistanceRunner())
        println(y.name)
        println(lazyValue)
        y.run()
    }
}