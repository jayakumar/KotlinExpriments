package com.example.jinuj.kotlintest1.kotlin.classes.objects

import org.junit.Test

/**
 * Created by jinu.j on 09-06-2017.
 **/
class NestedClassesUnitTest {

    class Outer {
        private val bar: Int = 1

        class Nested {
            fun foo() = 2
        }
    }


    class Outer1 {
        private val bar: Int = 1

        inner class Inner1 {
            fun foo() = bar
        }
    }


    interface X {
        fun `foo`()
    }

    abstract class Y : X

    @Test
    fun nestedStaticClass() {
        println(Outer.Nested().foo())
    }

    @Test
    fun nestedInnerClass() {
        println(Outer1().Inner1().foo())
    }

    @Test
    fun AnonymousInnerClasses() {
        object :Y(){
            override fun foo() {
                println("Foo")
            }
        }
    }
}