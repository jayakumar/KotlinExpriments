package com.example.jinuj.kotlintest1.kotlin.classes.objects

import org.junit.Test

/**
 * Created by jinu.j on 09-06-2017.
 **/
class EnumClassesUnitTest {

    enum class Direction {
        NORTH, SOUTH, WEST, EAST
    }

    enum class Color(val rgb: Int) {
        RED(0xFF0000),
        GREEN(0x00FF00),
        BLUE(0x0000FF)
    }

    enum class ProtocolState {
        WAITING {
            override fun signal() = TALKING
        },

        TALKING {
            override fun signal() = WAITING
        };

        abstract fun signal(): ProtocolState
    }


    @Test
    fun enumTest() {
        val dir = Direction.values().iterator()
        while (dir.hasNext()) {
            println(dir.next().name)
        }
    }

    @Test
    fun Initialization() {
        val z = Color.values().iterator()
        while (z.hasNext()) {
            val x = z.next()
            println("${x.rgb} rgb ${x.name} name")
        }
    }


    @Test
    fun AnonymousClasses() {
        val dir = ProtocolState.values().iterator()
        while (dir.hasNext()) {
            val z = dir.next()
            println("${z.signal()}")
        }
    }



}