package com.example.jinuj.kotlintest1.kotlin.basics

import org.junit.Test

/**
 * Created by jinu.j on 07-06-2017.
 **/

class BasicTypeUnitTests {

    @Test
    @Throws(Exception::class)
    fun Underscores_in_numeric_literals() {
        val oneMillion = 1_000_000
        val creditCardNumber = 1234_5678_9012_3456L
        val socialSecurityNumber = 999_99_9999L
        val hexBytes = 0xFF_EC_DE_5E
        val bytes = 0b11010010_01101001_10010100_10010010
        println(oneMillion)
        println(creditCardNumber)
        println(socialSecurityNumber)
        println(hexBytes)
        println(bytes)
    }

    @Test
    @Throws(Exception::class)
    fun Representation() {
        val a: Int = 10000
        println(a == a) // Prints 'true'
        val boxedA: Int? = a
        val anotherBoxedA: Int? = a
        println(boxedA === anotherBoxedA) // !!!Prints 'false'!!!
    }

    /**
     * NB : === checks if the objects are same
     * **/
    @Test
    @Throws(Exception::class)
    fun Representation1() {
        val a: Int? = 1000
        print(a == a) // Prints 'true'
        val boxedA: Int? = a
        val anotherBoxedA: Int? = a
        println(boxedA === anotherBoxedA) // Prints 'true'
    }

    @Test
    @Throws(Exception::class)
    fun Explicit_Conversions() {
        // Hypothetical code, does not actually compile:
        val a: Int? = 1 // A boxed Int (java.lang.Integer)
        val b: Long? = a?.toLong() // implicit conversion yields a boxed Long (java.lang.Long)
        println(a?.equals(b)) // Surprise! This prints "false" as Long's equals() check for other part

        val b1: Byte = 1 // OK, literals are checked statically
        val i: Int = b1.toInt() // OK: explicitly widened
        println(i)
        //Absence of implicit conversions is rarely noticeable because the type is inferred from the context,
        // and arithmetical operations are overloaded for appropriate conversions, for example
        val l = 1L + 3
        println(l)
    }

    @Test
    fun Operations() {
        val x = (1 shl 2) and 0x000FF000

        println(x)
    }


    @Test
    fun Characters() {
        var c = 'a'..'z'
        c.forEach { print(it) }
        c = 'A'..'Z'
        c.forEach { print(it) }
        println(c.toString())
    }

    @Test
    fun Booleans() {
        val x = "The type Boolean represents booleans, and has two values: true and false." +

                "Booleans are boxed if a nullable reference is needed." +

                "Built-in operations on booleans include" +

                "       || – lazy disjunction" +
                "       && – lazy conjunction" +
                "! - negation"
        print(x)
    }

    @Test
    fun Arrays() {
        val asc = Array(5, { i -> (i * i).toString() })
        asc.forEach { println(it) }

        val x: IntArray = intArrayOf(1, 2, 3)
        x[0] = x[1] + x[2]
        x.forEach { print(it) }
    }


    @Test
    fun Strings() {
        val str = "jinu"
        for (c in str) {
            println(c)
        }

        val s = "Hello, world!\n"
        println(s)
        val text = """
    |Tell me and I forget.
    |Teach me and I remember.
    |Involve me and I learn.
    |(Benjamin Franklin)
    """.trimMargin(">").trimIndent()
        println(text)

        val i = 10
        val s1 = "i = $i" // evaluates to "i = 10"
        println(s1)
        val s2 = "abc"
        val str1 = "$s2.length is ${s2.length}" // evaluates to "abc.length is 3"
        println(str1)
        val price = """
            ${'$'}9.99
            """
        println(price)
    }


}