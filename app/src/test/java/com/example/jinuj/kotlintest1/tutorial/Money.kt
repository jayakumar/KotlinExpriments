package com.example.jinuj.kotlintest1.tutorial

import java.math.BigDecimal

/**
 * Created by jinu.j on 12-06-2017.
 **/
data class Money(var amount: BigDecimal, var currency: String)

