package com.example.jinuj.kotlintest1.kotlin.others

import org.junit.Test

/**
 * Created by jinu.j on 14-06-2017.
 **/
class RangesUnitTest {

    @Test
    fun rangeTest() {
        for (i in 1..10) { // equivalent of 1 <= i && i <= 10
            println(i)
        }

        for (i in 1..4) print(i)

        for (i in 4..1) print(i) // prints nothing

        for (i in 4 downTo 1) print(i)

        for (i in 1..4 step 2) print(i) // prints "13"

        for (i in 4 downTo 1 step 2) print(i) // prints "42"

        for (i in 1 until 10) { // i in [1, 10), 10 is excluded
            println(i)
        }


    }

    @Test
    fun UtilityFunctions() {

    }


}