package com.example.jinuj.kotlintest1.kotlin.others

import org.junit.Test

/**
 * Created by jinu.j on 14-06-2017.
 **/
class TypeCastsUnitTest{

    @Test
    fun unsafe(){
        val y = "10"
        val x:String = y as String
        println(x)
    }


}