package com.example.jinuj.kotlintest1.kotlin.basics

import org.junit.Test
import java.lang.Integer.parseInt

/**
 * Created by jinu.j on 07-06-2017.
 **/
class ControlFlowUnitTest {


    @Test
    fun If_Expression() {
// Traditional usage
        val a = 10
        val b = 11
        var max = a
        if (a < b) max = b


        println(max)
// With else
        val max1: Int
        if (a > b) {
            max1 = a
        } else {
            max1 = b
        }

        println(max1)

// As expression
        val max2 = if (a > b) a else b

        println(max2)

        val max3 = if (a > b) {
            print("Choose a")
            a
        } else {
            print("Choose b")
            b
        }

        println(max3)
    }

    @Test
    fun When_Expression() {
        println()
        val x = 1
        when (x) {
            1 -> println("x == 1")
            2 -> println("x == 2")
            3 -> println("x == 3")
            4 -> println("x == 4")
            else -> { // Note the block
                println("x is neither 1 or 2,3,4")
            }
        }

        println()
        when (x) {
            0, 1 -> println("x == 0 or x == 1")
            else -> println("otherwise")
        }

        println()
        val s = "1"
        when (x) {
            parseInt(s) -> println("s encodes x")
            else -> println("s does not encode x")
        }

        println()
        val validNumbers = 30..40
        when (x) {
            in 1..10 -> println("x is in the range")
            in validNumbers -> println("x is valid")
            !in 10..20 -> println("x is outside the range")
            else -> println("none of the above")
        }

        println()
        fun hasPrefix(x: Any) = when (x) {
            is String -> x.startsWith("prefix")
            else -> false
        }

        println()
        when {
            x.isOdd() -> println("x is odd")
            x.isEven() -> println("x is even")
            else -> println("x is funny")
        }
        println(hasPrefix(x))

    }

    @Test
    fun For_Loops() {

        val collection = arrayOf("jinu", "gayathri", "bibin")
        for (item in collection) println(item)

        println()
        for (item: String in collection) {
            println(item)
        }

        println()
        for (s in 0 until collection.size) {
            println(collection[s])
        }

        println()
        for ((index, value) in collection.withIndex()) {
            println("the element at $index is $value")
        }

    }

    @Test
    fun While_Loops() {

    }


    //extension function
    private fun Int.isEven(): Boolean {
        return inc() - 1 % 2 == 0
    }


    //extension function
    private fun Int.isOdd(): Boolean {
        return inc() - 1 % 2 == 1
    }
}



