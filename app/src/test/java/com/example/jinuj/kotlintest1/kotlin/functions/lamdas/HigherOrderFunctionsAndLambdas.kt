package com.example.jinuj.kotlintest1.kotlin.functions.lamdas

import org.junit.Test

/**
 * Created by jinu.j on 13-06-2017.
 **/
class HigherOrderFunctionsAndLambdas {

    @Test
    fun HigherOrderFunctions() {
        val x = SQdb()
        x.lock({ x.doSomeThing("hi i did this") })

        val ints = arrayListOf(1, 2, 3)

        //it: implicit name of a single parameter
        val doubled = ints.map { it * 2 }
        doubled.forEach { println(it) }
        doubled.forEachIndexed { index, i ->
            println(" $index index $i ")
        }

        //it: implicit name of a single parameter
        x.lockImpName { it.doSomeThing("hi i did this") }


        //Inline Functions
        x.lockImpNoName { doSomeThing("1") }
                .lock { x.doSomeThing("2") }
                .lockImpName { it.doSomeThing("3") }

    }


    fun <T> SQdb.lock(body: () -> T): SQdb {
        this.beginTransaction()
        try {
            body()
        } finally {
            this.endTransaction()
            println()
            return this
        }
    }

    fun <T> SQdb.lockImpName(body: (SQdb) -> T): SQdb {
        this.beginTransaction()
        try {
            body(this)
        } finally {
            this.endTransaction()
            println()
            return this
        }
    }

    inline fun <T> SQdb.lockImpNoName(body: SQdb.() -> T): SQdb {
        this.beginTransaction()
        try {
            body(this)
        } finally {
            this.endTransaction()
            println()
            return this
        }
    }


    interface ClassData {
        fun beginTransaction()
        fun endTransaction()
        fun doSomeThing(string: String)
    }

    class SQdb : ClassData {
        override fun beginTransaction() {
            println("beginTransaction")
        }

        override fun endTransaction() {
            println("endTransaction")
        }

        override fun doSomeThing(string: String) {
            println(string)
        }
    }


    fun <T, R> List<T>.map(transform: (T) -> R): List<R> {
        val result = arrayListOf<R>()
        for (item in this)
            result.add(transform(item))
        return result
    }




}