package com.example.jinuj.kotlintest1.kotlin.classes.objects

import org.junit.Test
import kotlin.properties.Delegates
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

/**
 * Created by jinu.j on 12-06-2017.
 **/
class DelegatedPropertiesUnitTest {

    class Example {
        var p: String by Delegate()
    }

    class Delegate {
        operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
            return "$thisRef, thank you for delegating '${property.name}' to me!"
        }

        operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
            println("$value has been assigned to '${property.name} in $thisRef.'")
        }
    }

    val lazyValue: String by lazy {
        println("computed!")
        "Hello"
    }


    @Test
    fun DelegatedProperties() {
        val e = Example()
        println(e.p)
        e.p = "jinu"
        println(e.p)
    }


    @Test
    fun StandardDelegates() {
        println(lazyValue)
        println(lazyValue)
    }

    class User {
        var name: String by Delegates.observable("<no name>") {
            _, old, new ->
            println("$old -> $new")
        }
    }


    @Test
    fun Observable() {
        val user = User()
        user.name = "first"
        user.name = "second"
    }

    class User1(map: Map<String, Any>) {
        val name: String by map
        val age: Int     by map
    }

    val user = User1(mapOf(
            "name" to "John Doe",
            "age" to 25
    ))


    @Test
    fun StoringPropertiesInAMap() {
        println(user.name) // Prints "John Doe"
        println(user.age)  // Prints 25
    }

    @Test
    fun LocalDelegatedProperties() {
        val prop: String by MyDelegate()
        println(prop)
    }


    class MyDelegate : ReadOnlyProperty<String?, String> {
        override fun getValue(thisRef: String?, property: KProperty<*>): String {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }


    }

}


