package com.example.jinuj.kotlintest1.kotlin.classes.objects

import org.junit.Test

/**
 * Created by jinu.j on 08-06-2017.
 **/
class ExtensionsUnitTest {

    @Test
    fun ExtensionFunctions() {
        val x = MutableList(10, { it * it })
        x.forEach { print("$it ,") }

        println()
        x.swap(0, 5)
        x.forEach { print("$it ,") }

        println()
        val l = mutableListOf(1, 2, 3)
        l.swap(0, 2)
        l.forEach { print("$it ,") }

        println()
        printFoo(D())
    }

    @Test
    fun NullableReceiver() {
        var x: String? = null
        x = null
        x.toStrings()
    }

    @Test
    fun ExtensionProperties() {
        val x= listOf(1,2,4)
        println(x.lastIndex)
    }


    @Test
    fun CompanionObjectExtensions(){
        MyClass.foo()
    }

    @Test
    fun DeclaringExtensionsAsMembers(){
        C1().caller(D1())
    }



    class MyClass {
        companion object   // will be called "Companion"
    }

    fun MyClass.Companion.foo() {
        println("Test")
    }

    class D1 {
        fun bar() { println("bar") }
    }

    class C1 {
        fun baz() { println("baz") }

        fun D1.foo() {
            bar()   // calls D.bar
            baz()   // calls C.baz
        }

        fun caller(d: D1) {
            d.foo()   // call the extension function
        }
    }


    val <T> List<T>.lastIndex: Int
        get() = size - 1

    fun Any?.toStrings(): String {
        if (this == null) return "null"
        // after the null check, 'this' is autocast to a non-null type, so the toString() below
        // resolves to the member function of the Any class
        return toString()
    }

    open class C

    class D : C()

    fun C.foo() = "c"

    fun D.foo() = "d"

    fun printFoo(c: D) {
        println(c.foo())
    }


    fun MutableList<Int>.swap(index1: Int, index2: Int) {
        val tmp = this[index1] // 'this' corresponds to the list
        this[index1] = this[index2]
        this[index2] = tmp
    }
}