package com.example.jinuj.kotlintest1.kotlin.others

import org.junit.Test

/**
 * Created by jinu.j on 14-06-2017.
 **/
class OperatorOverloadingUnitTest {

    val x = Point(100, 101)

    data class Point(var x: Int, var y: Int)

    operator fun Point.plus(point: Point): Point {
        val x = this.x + point.x
        val y = this.y + point.y
        return Point(x, y)
    }

    operator fun Point.minus(point: Point): Point {
        val x = this.x - point.x
        val y = this.y - point.y
        return Point(x, y)
    }

    operator fun Point.unaryPlus() {
        this.x = +x
        this.y = +y
    }

    operator fun Point.unaryMinus() {
        this.x = -x
        this.y = -y
    }


    @Test
    fun unaryPlusTest() {
        +x
        var n = x
        println("${x.x}  ${x.y}")
        n += x
        println("${n.x}  ${n.y}")
    }


    @Test
    fun unaryMinusTest() {
        -x
        var n = x
        println("${x.x}  ${x.y}")
        n -= x
        println("${n.x}  ${n.y}")
    }


}