package com.example.jinuj.kotlintest1.kotlin.others

import org.junit.Test

/**
 * Created by jinu.j on 14-06-2017.
 **/
class ThisExpressionUnitTest{

    @Test
    fun ThisExpression(){
        val x= lamda@ fun String.():String{
            println(this)
            return "10"
        }

        println(x)
    }


    class A { // implicit label @A

        inner class B{

            fun Int.foo(){
                val a=this@A
                val b=this@B
                val c = this
                val c1 = this@foo
                val funLit = lambda@ fun String.() {
                    val d = this // funLit's receiver
                }
                val funLit2 = { s: String ->
                    // foo()'s receiver, since enclosing lambda expression
                    // doesn't have any receiver
                    val d1 = this
                }
            }
        }
    }
}