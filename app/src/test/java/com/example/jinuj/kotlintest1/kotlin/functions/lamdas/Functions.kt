package com.example.jinuj.kotlintest1.kotlin.functions.lamdas

import org.junit.Test

/**
 * Created by jinu.j on 13-06-2017.
 **/
class Functions {


    fun double(x: Int): Int {
        return 2 * x
    }

    val result = double(2)

    infix fun Int.mult(x: Int): Int {
        return this * x
    }

    @Test
    fun testPrefix() {
        println(10.mult(3))
        println(result)
        println(10 mult 3)
    }

    @Test
    fun LocalFunctions() {

    }


    @Test
    fun TailRecursiveFunctions(){
        println(findFixPoint(5.0))
    }

    tailrec fun findFixPoint(x: Double = 1.0): Double
            = if (x == Math.cos(x)) x else findFixPoint(Math.cos(x))
}