package com.example.jinuj.kotlintest1.kotlin.classes.objects

import org.junit.Assert
import org.junit.Test

/**
 * Created by jinu.j on 07-06-2017.
 **/
class PropertiesAndFieldsUnitTest {

    var size = 0
    val isEmpty: Boolean
        get() = this.size == 0

    private var actualVal: String? = null
    var stringRepresentation: String?
        get() = actualVal
        set(value) {
            actualVal = setDataFromString(value) // parses the string and assigns values to other properties
        }

    private var _table: Map<String, Int>? = null
    public val table: Map<String, Int>
        get() {
            if (_table == null) {
                _table = HashMap() // Type parameters are inferred
            }
            return _table ?: throw AssertionError("Set to null by another thread")
        }


    companion object {
        var setterVisibility: String = "abc"
            private set // the setter is private and has the default implementation
    }

    private fun setDataFromString(value: String?): String? {
        return value + " jinu"
    }

    open class Address {
        var name: String? = null
        var street: String? = null
        var city: String? = null
        var state: String? = null
        var zip: String? = null
    }


    fun copyAddress(address: Address): Address {
        val result = Address() // there's no 'new' keyword in Kotlin
        result.name = address.name // accessors are called
        result.street = address.street
        result.city = address.city
        result.state = address.state
        result.zip = address.zip
        return result
    }

    @Test
    fun copyAddressTest() {
        val address = Address()
        address.name = "jinu"
        address.street = "sreekaryam"
        address.city = "tvm"
        address.state = "kerala"
        address.zip = "695588"
        val result = copyAddress(address)
        Assert.assertNotEquals(result, address)
    }


    @Test
    fun GettersAndSetters() {
        Assert.assertTrue(isEmpty)
        stringRepresentation = "Hi"
        Assert.assertEquals(stringRepresentation, "Hi jinu")
        Test1()
    }

    @Test
    fun BackingProperties() {
        val bool = _table ?: false
        println(bool is Boolean)
    }


    class Test1 {
        init {
            Companion.setterVisibility = "x"
            println(Companion.setterVisibility)
        }
    }
}