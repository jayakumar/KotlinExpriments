package com.example.jinuj.kotlintest1.kotlin.classes.objects

import org.junit.Test

/**
 * Created by jinu.j on 08-06-2017.
 **/
class GenericsUnitTest {


    class Box<T>(t: T) {
        var value = t
    }



    @Test
    fun generics() {
        val box: Box<Int> = Box(1)
        val box1: Box<String> = Box("iuih")
        println(box.value)
        println(box1.value)
    }

    @Test
    fun variance() {
        val first = mutableListOf("jinu", "gayathri", "bibin")
        val second: MutableCollection<Any> = mutableListOf("jinu", 1, "bibin")
        copyAll(second, first)
        println("------------------first---------------")
        first.forEach { print("$it ,") }
        println("------------------second--------------")
        second.forEach { print("$it ,") }
    }


    fun copyAll(to: MutableCollection<Any>, from: Collection<String>) {
        to.addAll(from) // !!! Would not compile with the naive declaration of addAll:
        //       Collection<String> is not a subtype of Collection<Object>
    }

}