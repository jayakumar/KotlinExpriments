package com.example.jinuj.kotlintest1.kotlin.classes.objects

import org.junit.Test

/**
 * Created by jinu.j on 08-06-2017.
 **/
class DataClassUnitTest {


    data class User(val name: String, val age: Int)



    @Test
    fun dataClass() {
        val user=User("jinu",23)
        val olderUser=user.copy(age = 24)
        println(user.toString())
        println(olderUser.toString())
    }
}