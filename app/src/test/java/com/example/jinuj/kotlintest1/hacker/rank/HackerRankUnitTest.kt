package com.example.jinuj.kotlintest1.hacker.rank

import org.junit.Test

/**
 * Created by jinu.j on 15-06-2017.
 **/
class HackerRankUnitTest {

    val x = arrayOf(2, -1, 2, 3, 4, -5)
    val y = arrayOf(1, 2, 3, 4)

    @Test
    fun TheMaximumSubArray() {
        val x1 = HashMap<Int, Int>()
        x.forEachIndexed { index, i -> x1.put(index, i) }
        x1.forEach { println("${it.key} ${it.value}") }
        javaTest(0, x1, arrayListOf(x), false)
    }


    fun javaTest(n: Int = 0, array: HashMap<Int, Int>, arrayInts: List<Array<Int>>? = null, runNext: Boolean) {
        if (n == x.size) {
            array.forEach {
                println()
                it.to(print("${it.key} ${it.value} "))
            }
            return
        }

        for (i in n until x.size - 1) {
            if (i <= n) {
                array.put(i, x[i])
            }

        }

        array.forEach {
            javaTest(n + 1, array, arrayInts, false)
        }

    }
}