package com.example.jinuj.kotlintest1.tutorial

import org.junit.Test
import java.math.BigDecimal

/**
 * Created by jinu.j on 12-06-2017.
 **/
class Main {
    private val Int.bd: BigDecimal
        get() = BigDecimal(this)
    @Test
    fun doSomething() {
        val tickets = Money(33.bd, "$")
        val popCorn = tickets.copy(500.bd, currency = "EUR")
        sendPayment(tickets, "Good luck")
        if (popCorn === tickets) {
            println("They are different")
        }
        convertToDollars(popCorn)
        println(tickets.amount.percent(50))
        println(7 percentOf tickets)
        val bd2 = 100.bd
        println(bd2)
    }


    fun javaMoney(money: Money?) {

        println("${money?.amount}is valid")
    }

    fun BigDecimal.percent(percentage: Int): BigDecimal = this.multiply(java.math.BigDecimal(percentage)).divide(BigDecimal(100))

    infix fun Int.percentOf(money: Money): BigDecimal = money.amount.multiply(BigDecimal(this)).divide(BigDecimal(100))

    fun sendPayment(money: Money, message: String = "") {
        println("Sending ${money.amount} message $message")
    }

    fun convertToDollars(money: Money): Money {
        return when (money.currency) {
            "$" -> money
            "EUR" -> Money(money.amount * BigDecimal(1.10), "$")
            else -> throw IllegalArgumentException()
        }
    }
}


