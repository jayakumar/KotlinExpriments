package com.example.jinuj.kotlintest1.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

/**
 * Created by jinu.j on 02-06-2017.
 **/

@Database(entities = arrayOf(UserEntity::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    companion object{
        val DATABASE_NAME = "basic-sample-db"
    }

    abstract fun userDao(): UserDao
}