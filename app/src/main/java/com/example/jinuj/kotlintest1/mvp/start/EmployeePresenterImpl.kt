package com.example.jinuj.kotlintest1.mvp.start

import android.app.Activity
import com.example.jinuj.kotlintest1.model.Employee

/**
 * Created by jinu.j on 25-05-2017.
 **/
class EmployeePresenterImpl(private var view: EmployeeListView?) : EmployeePresenter,
        EmployeePresenter.OnLoadEmployee, EmployeePresenter.LocationResult {


    private var employee: EmployeeInteractor = EmployeeInteractorImpl()

    override fun getEmployee() {
        employee.getEmployees(this)
    }

    override fun displayEmployee(mEmployee: Employee?) {
        if (view == null) {
            return
        }
        view!!.displayProgress()
        view!!.displayEmployee(mEmployee)
    }

    override fun displayError(error: String) {
        if (view == null) {
            return
        }
        view!!.displayError(error)
    }


    override fun enableLocation(activity: Activity) {
        if (view == null) {
            return
        }
        employee.enableLocation(activity, this)
    }

    override fun onDestroy() {
        view = null
    }

    override fun askLocationPermission() {
        if (view == null) {
            return
        }
        view!!.askLocationPermission()
    }

    override fun enableLocation() {
        if (view == null) {
            return
        }
        view!!.enableLocation()
    }
}