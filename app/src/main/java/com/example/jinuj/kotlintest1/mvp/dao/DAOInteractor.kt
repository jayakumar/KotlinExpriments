package com.example.jinuj.kotlintest1.mvp.dao

import android.content.Context
import com.example.jinuj.kotlintest1.db.UserEntity

/**
 * Created by jinu.j on 05-06-2017.
 **/
interface DAOInteractor {

    fun insertData(userEntity: UserEntity?, context: Context,onInsertEvent: DAOPresenter.OnInsertEvent?)

}