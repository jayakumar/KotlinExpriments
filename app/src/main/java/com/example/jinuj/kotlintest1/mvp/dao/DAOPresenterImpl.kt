package com.example.jinuj.kotlintest1.mvp.dao

import android.content.Context
import android.text.TextUtils
import com.example.jinuj.kotlintest1.db.UserEntity

/**
 * Created by jinu.j on 05-06-2017.
 **/
class DAOPresenterImpl(private var view: DAOView? = null) : DAOPresenter, DAOPresenter.OnInsertEvent {


    private var mDAOInteractor: DAOInteractor? = null

    init {
        mDAOInteractor = DAOInteractorImpl()
    }

    override fun insertUser(name: String?, email: String?, phNo: String?, address: String?, context: Context) {
        var hasError = false
        if (TextUtils.isEmpty(name)) {
            view?.userNameError(DAOView.UserErrType.EMPTY)
            hasError = true
        } else {
            view?.userNameError(DAOView.UserErrType.NO_ERROR)
        }
        if (TextUtils.isEmpty(email)) {
            view?.emailIdError(DAOView.UserErrType.EMPTY)
            hasError = true
        } else {
            view?.emailIdError(DAOView.UserErrType.NO_ERROR)
        }
        if (TextUtils.isEmpty(phNo)) {
            view?.phNoError(DAOView.UserErrType.EMPTY)
            hasError = true
        } else {
            view?.phNoError(DAOView.UserErrType.NO_ERROR)
        }
        if (TextUtils.isEmpty(address)) {
            view?.addressError(DAOView.UserErrType.EMPTY)
            hasError = true
        } else {
            view?.addressError(DAOView.UserErrType.NO_ERROR)
        }

        if (hasError) {
            return
        }

        val entity = UserEntity()
        entity.setName(name)
        entity.setPhNo(phNo)
        entity.setEmail(email)
        entity.setAddress(address)
        mDAOInteractor?.insertData(entity, context, this)
    }


    override fun onInsertSuccess() {
        view?.insertComplete()
    }

    override fun onInsertFail() {
        view?.insertFail()
    }

}