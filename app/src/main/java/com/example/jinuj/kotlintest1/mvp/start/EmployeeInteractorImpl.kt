package com.example.jinuj.kotlintest1.mvp.start

import android.Manifest
import android.support.v4.content.ContextCompat
import com.example.jinuj.kotlintest1.api.RetrofitProvider
import com.example.jinuj.kotlintest1.api.UserApi
import com.example.jinuj.kotlintest1.model.Employee
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by jinu.j on 25-05-2017.
 **/
class EmployeeInteractorImpl : EmployeeInteractor, Callback<Employee> {


    lateinit private var onLoadEmployee: EmployeePresenter.OnLoadEmployee


    override fun getEmployees(onLoadEmployee: EmployeePresenter.OnLoadEmployee) {
        this.onLoadEmployee = onLoadEmployee

        getUser()
    }

    private fun getUser() {
        val userApi = RetrofitProvider.Companion.instance.retroFit.create(UserApi::class.java)
        val api: Call<Employee>? = userApi.getUser()
        api!!.enqueue(this)
    }


    override fun onFailure(call: retrofit2.Call<Employee>?, t: Throwable?) {
        onLoadEmployee.displayError(t!!.localizedMessage)
    }

    override fun onResponse(call: retrofit2.Call<Employee>?, response: Response<Employee>?) {
        if (response!!.body() != null) {
            onLoadEmployee.displayEmployee(response.body())
        }
    }

    override fun enableLocation(activity: android.app.Activity,
                                mLocationFetcher: EmployeePresenter.LocationResult) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION)
                != android.content.pm.PackageManager.PERMISSION_GRANTED) {
            mLocationFetcher.askLocationPermission()
            return
        }
        mLocationFetcher.enableLocation()
    }

}