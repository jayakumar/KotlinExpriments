package com.example.jinuj.kotlintest1.mvp.start

import android.app.Activity
import com.example.jinuj.kotlintest1.model.Employee

/**
 * Created by jinu.j on 25-05-2017.
 **/
interface EmployeePresenter {

    fun getEmployee()

    fun onDestroy()

    fun enableLocation(activity: Activity)



    interface OnLoadEmployee {
        fun displayEmployee(mEmployee: Employee?)

        fun displayError(error: String)
    }

    interface LocationResult {
        fun askLocationPermission()

        fun enableLocation()
    }
}