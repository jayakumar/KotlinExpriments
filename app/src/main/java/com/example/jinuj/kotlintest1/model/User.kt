@file:Suppress("unused")

package com.example.jinuj.kotlintest1.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by jinu.j on 24-05-2017.
 **/

open class User : Parcelable {

    @SerializedName("name")
    lateinit var name: String

    @SerializedName("age")
    lateinit var age: String

    @SerializedName("email")
    lateinit var email: String

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(this.name)
        dest.writeString(this.age)
        dest.writeString(this.email)
    }

    constructor()

    protected constructor(`in`: Parcel) {
        this.name = `in`.readString()
        this.age = `in`.readString()
        this.email = `in`.readString()
    }

    companion object {

        val CREATOR: Parcelable.Creator<User> = object : Parcelable.Creator<User> {
            override fun createFromParcel(source: Parcel): User {
                return User(source)
            }

            override fun newArray(size: Int): Array<User?> {
                return arrayOfNulls(size)
            }
        }
    }
}
