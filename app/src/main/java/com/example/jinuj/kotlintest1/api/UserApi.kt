package com.example.jinuj.kotlintest1.api

import com.example.jinuj.kotlintest1.model.Category
import com.example.jinuj.kotlintest1.model.Employee
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.QueryMap

/**
 * Created by jinu.j on 24-05-2017.
 **/

interface UserApi {

    @GET("master/sample_json/list.json")
    fun getUser(): Call<Employee>

    @POST("categoryList")
    fun categoryList(@QueryMap userId: HashMap<String, String>): Call<Category>
}
