package com.example.jinuj.kotlintest1.mvp.start

import android.app.Activity


/**
 * Created by jinu.j on 25-05-2017.
 **/
interface EmployeeInteractor {

    fun getEmployees(onLoadEmployee: EmployeePresenter.OnLoadEmployee)

    fun enableLocation(activity: Activity, mLocationFetcher: EmployeePresenter.LocationResult)


}