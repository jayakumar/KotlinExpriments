@file:Suppress("unused")

package com.example.jinuj.kotlintest1.model

import android.os.Parcel
import android.os.Parcelable

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class Category : Parcelable {

    @SerializedName("success")
    @Expose
    var success: String? = null


    @SerializedName("result")
    @Expose
    var result: Result? = null


    @SerializedName("errorMessage")
    @Expose
    var errorMessage: String? = null

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(this.success)
        dest.writeParcelable(this.result, flags)
        dest.writeString(this.errorMessage)
    }

    constructor()

    protected constructor(`in`: Parcel) {
        this.success = `in`.readString()
        this.result = `in`.readParcelable<Result>(Result::class.java.classLoader)
        this.errorMessage = `in`.readString()
    }

    companion object {

        val CREATOR: Parcelable.Creator<Category> = object : Parcelable.Creator<Category> {
            override fun createFromParcel(source: Parcel): Category {
                return Category(source)
            }

            override fun newArray(size: Int): Array<Category?> {
                return arrayOfNulls(size)
            }
        }
    }
}
