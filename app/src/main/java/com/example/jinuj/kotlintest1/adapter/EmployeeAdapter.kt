package com.example.jinuj.kotlintest1.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.jinuj.kotlintest1.R
import com.example.jinuj.kotlintest1.model.Employee

/**
 * Created by jinu.j on 25-05-2017.
 **/
class EmployeeAdapter(private var mEmployee: Employee?) : RecyclerView.Adapter<EmployeeAdapter.EmployeeVH>() {


    override fun getItemCount(): Int = mEmployee!!.employees.size


    override fun onCreateViewHolder(viewGroup: ViewGroup?, pos: Int): EmployeeVH {
        val view = LayoutInflater.from(viewGroup!!.context).inflate(R.layout.employee_item, viewGroup,false)
        return EmployeeVH(view)
    }

    override fun onBindViewHolder(employeeVH: EmployeeVH, viewType: Int) {
        assert(mEmployee != null)
        employeeVH.mPhNo.text = mEmployee!!.employees[viewType].age
        employeeVH.mUserName.text = mEmployee!!.employees[viewType].name
        employeeVH.mEmail.text = mEmployee!!.employees[viewType].email
    }


    class EmployeeVH(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var mUserName: TextView = itemView!!.findViewById(R.id.name)
        var mEmail: TextView = itemView!!.findViewById(R.id.email)
        var mPhNo: TextView = itemView!!.findViewById(R.id.phno)
    }
}