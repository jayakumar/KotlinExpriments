package com.example.jinuj.kotlintest1.activity

import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.example.jinuj.kotlintest1.R
import com.example.jinuj.kotlintest1.mvp.dao.DAOPresenter
import com.example.jinuj.kotlintest1.mvp.dao.DAOPresenterImpl
import com.example.jinuj.kotlintest1.mvp.dao.DAOView

class DAOActivity : AppCompatActivity(), DAOView {

    private var mName: EditText? = null
    private var mEmailId: EditText? = null
    private var mPhoneNo: EditText? = null
    private var mAddress: EditText? = null
    private var mPhoneNoContainer: TextInputLayout? = null
    private var mNameContainer: TextInputLayout? = null
    private var mEmailIdContainer: TextInputLayout? = null
    private var mAddressContainer: TextInputLayout? = null
    private var daoPresenter: DAOPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dao)
        daoPresenter = DAOPresenterImpl(this)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val fab = findViewById<View>(R.id.fab)
        mName = findViewById(R.id.name)
        mEmailId = findViewById(R.id.email)
        mPhoneNo = findViewById(R.id.phone_number)
        mAddress = findViewById(R.id.address)
        mNameContainer = findViewById(R.id.name_container)
        mEmailIdContainer = findViewById(R.id.email_container)
        mPhoneNoContainer = findViewById(R.id.phone_number_container)
        mAddressContainer = findViewById(R.id.address_container)
        setSupportActionBar(toolbar)
        fab.setOnClickListener {
            daoPresenter?.insertUser(getString(mName),
                    getString(mEmailId),
                    getString(mPhoneNo),
                    getString(mAddress),
                    this@DAOActivity)
        }
    }

    private fun getString(editText: EditText?): String? {
        return editText?.text.toString()
    }

    private fun disableError(mContainer: TextInputLayout?) {
        mContainer?.isErrorEnabled = false
    }


    override fun userNameError(error: Int) {
        if (error != DAOView.UserErrType.NO_ERROR) {
            mNameContainer?.error = getString(R.string.error_name)
        } else {
            disableError(mNameContainer)
        }
    }

    override fun emailIdError(error: Int) {
        if (error != DAOView.UserErrType.NO_ERROR) {
            mEmailIdContainer?.error = getString(R.string.error_email)
        } else {
            disableError(mEmailIdContainer)
        }
    }

    override fun addressError(error: Int) {
        if (error != DAOView.UserErrType.NO_ERROR) {
            mAddressContainer?.error = getString(R.string.error_address)
        } else {
            disableError(mAddressContainer)
        }
    }

    override fun phNoError(error: Int) {
        if (error != DAOView.UserErrType.NO_ERROR) {
            mPhoneNoContainer?.error = getString(R.string.error_ph_no)
        } else {
            disableError(mPhoneNoContainer)
        }
    }

    override fun insertComplete() {
        Toast.makeText(this, getString(R.string.insert_success), Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun insertFail() {
        Toast.makeText(this, getString(R.string.insert_failed), Toast.LENGTH_SHORT).show()
    }


}
