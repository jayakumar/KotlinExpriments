package com.example.jinuj.kotlintest1.location

import android.location.Location

/**
 * Created by jinu.j on 01-06-2017.
 **/
interface LocationReceiver {
    fun onGetLocation(location: Location?)
}