package com.example.jinuj.kotlintest1.mvp.dao

import android.content.Context

/**
 * Created by jinu.j on 05-06-2017.
 **/
interface DAOPresenter {

    interface OnInsertEvent{
        fun onInsertSuccess()
        fun onInsertFail()
    }

    fun insertUser(name: String?, email: String?, phNo: String?, address: String?, context: Context)
}