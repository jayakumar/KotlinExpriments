package com.example.jinuj.kotlintest1.mvp.dao

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import com.example.jinuj.kotlintest1.db.DbProvider
import com.example.jinuj.kotlintest1.db.UserEntity

/**
 * Created by jinu.j on 05-06-2017.
 **/
class DAOInteractorImpl : DAOInteractor {


    override fun insertData(userEntity: UserEntity?, context: Context, onInsertEvent: DAOPresenter.OnInsertEvent?) {
        val s = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<Void, Void, Boolean>() {
            override fun doInBackground(vararg p0: Void?): Boolean {
                try {
                    DbProvider.getInstance(context)?.db?.userDao()?.insertAll(userEntity)
                } catch (e: Exception) {
                    return false
                }
                return true
            }

            override fun onPostExecute(result: Boolean) {
                super.onPostExecute(result)
                if (result) {
                    onInsertEvent?.onInsertSuccess()
                } else {
                    onInsertEvent?.onInsertFail()
                }
            }
        }
        s.execute()
    }


}