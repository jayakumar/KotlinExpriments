package com.example.jinuj.kotlintest1.mvp.start

import com.example.jinuj.kotlintest1.model.Employee

/**
 * Created by jinu.j on 25-05-2017.
 **/
interface EmployeeListView {

    fun displayEmployee(mEmployee: Employee?)

    fun displayError(error: String)

    fun displayProgress()


    fun askLocationPermission()

    fun enableLocation()

}