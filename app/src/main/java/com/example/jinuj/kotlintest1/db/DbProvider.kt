package com.example.jinuj.kotlintest1.db

import android.arch.persistence.room.Room
import android.content.Context

/**
 * Created by jinu.j on 02-06-2017.
 **/

class DbProvider private constructor(context: Context) {

    var db: AppDatabase? = null
        private set

    init {
        if (db == null) {
            db = Room.databaseBuilder(context.applicationContext,
                    AppDatabase::class.java,
                   AppDatabase.DATABASE_NAME).build()
        }
    }

    companion object {
        private var ourInstance: DbProvider? = null

        fun getInstance(context: Context): DbProvider? {
            if (ourInstance == null) {
                ourInstance = DbProvider(context)
            }
            return ourInstance
        }
    }
}
