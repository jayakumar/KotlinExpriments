package com.example.jinuj.kotlintest1.provider

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.location.Location
import com.example.jinuj.kotlintest1.model.Employee

/**
 * Created by jinu.j on 31-05-2017.
 **/
class EmployeeDataViewModel : ViewModel() {


    private var liveLocation = MutableLiveData<Location>()

    private var employee: Employee? = null

    fun getEmployee(): Employee? {
        return employee
    }

    fun setEmployee(employee: Employee?) {
        this.employee = employee
    }

    fun getLocation(): LiveData<Location>? {
        return liveLocation
    }

    fun setLocation(location: Location?) {
        liveLocation.value = location
    }


}