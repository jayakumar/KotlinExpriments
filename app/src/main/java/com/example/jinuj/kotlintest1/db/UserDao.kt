package com.example.jinuj.kotlintest1.db

import android.arch.persistence.room.*
import android.database.Cursor


/**
 * Created by jinu.j on 02-06-2017.
 **/
@Dao
interface UserDao {

    @Query("SELECT * FROM user")
    fun getAll(): List<UserEntity>

    @Query("SELECT * FROM user")
    fun getAllUser(): Cursor

    @Query("SELECT * FROM user WHERE id IN (:arg0)")
    fun loadAllByIds(userIds: IntArray): List<UserEntity>


    @Query("SELECT * FROM user WHERE id IN (:arg0)")
    fun loadAllUserByIds(userIds: Long): Cursor

    @Update
    fun updateUser(vararg userEntity: UserEntity):Int

    @Insert
    fun insertAll(vararg userEntities: UserEntity?): List<Long>

    @Delete
    fun delete(userEntity: UserEntity)

}