package com.example.jinuj.kotlintest1.fragments


import android.arch.lifecycle.LifecycleFragment
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.example.jinuj.kotlintest1.R
import com.example.jinuj.kotlintest1.provider.LiveDataViewModel


/**
 * A simple [Fragment] subclass.
 */
class LiveProgressFragment : LifecycleFragment(), SeekBar.OnSeekBarChangeListener {


    private var mProgress: SeekBar? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragemtn_live_progress, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mProgress = view?.findViewById(R.id.progress)
        mProgress?.setOnSeekBarChangeListener(this)
        LiveDataViewModel.position?.observe(this, Observer<Int> {
            mProgress?.progress = it ?: 0
        })
    }


    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        if (fromUser) {
            LiveDataViewModel.setPosition(progress)
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }


    override fun onStopTrackingTouch(seekBar: SeekBar?) {

    }


}
