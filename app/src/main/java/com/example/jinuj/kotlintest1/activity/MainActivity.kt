package com.example.jinuj.kotlintest1.activity

import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.LifecycleActivity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.example.errorprogress.ErrorProgressView
import com.example.jinuj.kotlintest1.R
import com.example.jinuj.kotlintest1.adapter.EmployeeAdapter
import com.example.jinuj.kotlintest1.db.DbProvider
import com.example.jinuj.kotlintest1.location.LocationFetcher
import com.example.jinuj.kotlintest1.location.LocationReceiver
import com.example.jinuj.kotlintest1.model.Employee
import com.example.jinuj.kotlintest1.mvp.start.EmployeeListView
import com.example.jinuj.kotlintest1.mvp.start.EmployeePresenter
import com.example.jinuj.kotlintest1.mvp.start.EmployeePresenterImpl
import com.example.jinuj.kotlintest1.provider.EmployeeDataViewModel


class MainActivity : LifecycleActivity(), ErrorProgressView.OnRetryListener,
        EmployeeListView {


    lateinit private var mErrorProgressView: ErrorProgressView
    lateinit private var mRecyclerView: RecyclerView
    private var mEmployeeAdapter: EmployeeAdapter? = null
    lateinit private var mEmpPresenter: EmployeePresenter
    private var mDataProvider: EmployeeDataViewModel? = null
    private var mFAB: FloatingActionButton? = null
    val TAG = MainActivity::class.java.simpleName!!
    private var task: AsyncTask<Void, Void, Void>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mErrorProgressView = findViewById(R.id.errorProgressView)
        mRecyclerView = findViewById(R.id.recyclerView)
        mFAB = findViewById(R.id.floatingActionButton)
        mEmpPresenter = EmployeePresenterImpl(this)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mErrorProgressView.mOnRetryListener = this
        lifecycle.addObserver(LocationFetcher.getInstance(lifecycle))
        mDataProvider = ViewModelProviders.of(this).get(EmployeeDataViewModel::class.java)
        if (mDataProvider?.getEmployee() == null) {
            mEmpPresenter.getEmployee()
        } else {
            displayEmployee(mDataProvider?.getEmployee())
        }
        mEmpPresenter.enableLocation(this)
        mFAB?.setOnClickListener { printData()  }
        subScribe()
    }

    private fun launchDAOActivity() {
        val intent = Intent(this, DAOActivity::class.java)
        startActivity(intent)

    }


    private fun printData() {
        task = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<Void, Void, Void>() {
            override fun doInBackground(vararg p0: Void?): Void? {
                val data = DbProvider.getInstance(this@MainActivity)?.db?.userDao()?.getAll()
                if (data != null) {
                    for (i in 0 until data.size) {
                        println(data[i].getAddress())
                    }
                }
                return null
            }

            override fun onPostExecute(result: Void?) {
                super.onPostExecute(result)
                launchDAOActivity()
            }


        }
        task?.execute()
    }

    private fun subScribe() {
        mDataProvider?.getLocation()?.observe(this, Observer<Location> {
            Log.d(TAG, "" +
                    "")
        })
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mEmpPresenter.enableLocation(this)
            }
        }
    }

    override fun displayEmployee(mEmployee: Employee?) {
        mDataProvider?.setEmployee(mEmployee)
        mErrorProgressView.hideView()
        mEmployeeAdapter = EmployeeAdapter(mDataProvider?.getEmployee())
        mRecyclerView.adapter = mEmployeeAdapter
    }

    override fun displayError(error: String) {
        mErrorProgressView.showView()
        mErrorProgressView.setSubTitle(error)
        mErrorProgressView.showError()
        mErrorProgressView.visibility = View.VISIBLE
        println(error)
    }

    override fun displayProgress() {
        mErrorProgressView.showView()
        mErrorProgressView.showProgress()
    }


    override fun onRetry() {
        mEmpPresenter.getEmployee()
    }

    override fun onDestroy() {
        super.onDestroy()
        mEmpPresenter.onDestroy()
    }

    override fun enableLocation() {
        LocationFetcher.getInstance(lifecycle)!!.enableApi(this, object : LocationReceiver {
            override fun onGetLocation(location: Location?) {
                mDataProvider?.setLocation(location)
            }
        })
    }


    override fun askLocationPermission() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), 100)
    }


}


