@file:Suppress("unused")

package com.example.jinuj.kotlintest1.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.content.ContentValues

/**
 * Created by jinu.j on 02-06-2017.
 **/
@Entity(tableName = "user")
class UserEntity : User {


    constructor()


    companion object {
        fun fromContentValues(contentValues: ContentValues?): UserEntity? {
            val userEntity = UserEntity()
            userEntity.setName(contentValues?.getAsString("name"))
            userEntity.setPhNo(contentValues?.getAsString("phNo"))
            userEntity.setEmail(contentValues?.getAsString("email"))
            userEntity.setAddress(contentValues?.getAsString("address"))
            return userEntity
        }
    }

    constructor(user: User) {
        id = user.getId()
        name = user.getName()
        email = user.getEmail()
        address = user.getAddress()
        phNo = user.getPhNo()
    }

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private var id: Int? = null

    @ColumnInfo(name = "name")
    private var name: String? = null

    @ColumnInfo(name = "email")
    private var email: String? = null

    @ColumnInfo(name = "phNo")
    private var phNo: String? = null

    @ColumnInfo(name = "address")
    private var address: String? = null


    override fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

    override fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    override fun getEmail(): String? {
        return email
    }

    fun setEmail(email: String?) {
        this.email = email
    }

    fun setPhNo(phNo: String?) {
        this.phNo = phNo
    }

    override fun getPhNo(): String? {
        return phNo
    }


    fun setAddress(address: String?) {
        this.address = address
    }

    override fun getAddress(): String? {
        return address
    }


}