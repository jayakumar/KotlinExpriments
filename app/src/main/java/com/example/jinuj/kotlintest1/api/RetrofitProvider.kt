package com.example.jinuj.kotlintest1.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by jinu.j on 24-05-2017.
 **/

class RetrofitProvider private constructor() {


    private val mBuilder: Retrofit = Retrofit.Builder()
            .baseUrl("https://raw.githubusercontent.com/jinujayakumar/android-path-animation/")
            .addConverterFactory(GsonConverterFactory.create()).build()


    val retroFit: Retrofit
        get() {
            return mBuilder
        }

    companion object {
        internal val instance = RetrofitProvider()
    }
}
