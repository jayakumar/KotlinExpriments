package com.example.jinuj.kotlintest1.provider

import android.content.ContentProvider
import android.content.ContentUris
import android.content.ContentValues
import android.content.UriMatcher
import android.net.Uri
import com.example.jinuj.kotlintest1.db.DbProvider
import com.example.jinuj.kotlintest1.db.UserDao
import com.example.jinuj.kotlintest1.db.UserEntity


@Suppress("unused")
/**
 * Created by jinu.j on 05-06-2017.
 **/
class UserContentProvider : ContentProvider() {


    private var dao: UserDao? = null

    companion object {
        /** The authority of this content provider.  */
        val AUTHORITY = "com.example.jinuj.kotlintest1.provider"

        val TABLE_NAME = "user"

        @Suppress("HasPlatformType")
                /** The URI for the user table.  */
        val URI_USER = Uri.parse("content://$AUTHORITY/$TABLE_NAME")

        /** The match code for some items in the user table.  */
        private val CODE_USER_DIR = 1

        /** The match code for an item in the user table.  */
        private val CODE_USER_ITEM = 2

        /** The URI matcher.  */
        private val MATCHER = UriMatcher(UriMatcher.NO_MATCH)

    }


    init {
        MATCHER.addURI(AUTHORITY, TABLE_NAME, CODE_USER_DIR)
        MATCHER.addURI(AUTHORITY, TABLE_NAME + "/*", CODE_USER_ITEM)
    }


    override fun insert(uri: Uri?, values: ContentValues?): Uri {
        val type = MATCHER.match(uri)
        if (type == CODE_USER_DIR) {

            val id = dao?.insertAll(UserEntity.fromContentValues(values))
            context.contentResolver.notifyChange(uri, null)
            return ContentUris.withAppendedId(uri, id!![0])
        }
        throw IllegalArgumentException()
    }

    override fun query(uri: Uri?, projection: Array<out String>?, selection: String?,
                       selectionArgs: Array<out String>?, sortOrder: String?) =
            when (MATCHER.match(uri)) {
                CODE_USER_DIR -> dao?.getAllUser()
                CODE_USER_ITEM -> dao?.loadAllUserByIds(ContentUris.parseId(uri))
                else -> throw IllegalArgumentException("This operation cannot be proceeded")
            }

    override fun onCreate():Boolean{
        dao = DbProvider.getInstance(context)?.db?.userDao()
       return true
    }


    override fun update(uri: Uri?, values: ContentValues?, selection: String?, selArgs: Array<out String>?): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun delete(uri: Uri?, selection: String?, selArgs: Array<out String>?): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getType(uri: Uri?): String {
        when (MATCHER.match(uri)) {
            CODE_USER_DIR -> return "vnd.android.cursor.dir/$AUTHORITY.$TABLE_NAME"
            CODE_USER_ITEM -> return "vnd.android.cursor.item/$AUTHORITY.$TABLE_NAME"
        }
        throw IllegalArgumentException("unknown uri" + uri)
    }

}