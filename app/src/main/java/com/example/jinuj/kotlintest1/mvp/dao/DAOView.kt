@file:Suppress("unused")

package com.example.jinuj.kotlintest1.mvp.dao

/**
 * Created by jinu.j on 05-06-2017.
 **/
interface DAOView {

    interface UserErrType {
        companion object {
            val EMPTY = 0
            var INVALID = 1
            var NO_ERROR = 2
        }
    }

    fun userNameError(error: Int)

    fun emailIdError(error: Int)

    fun addressError(error: Int)

    fun phNoError(error: Int)

    fun insertComplete()

    fun insertFail()



}