package com.example.jinuj.kotlintest1.provider

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

/**
 * Created by jinu.j on 16-06-2017.
 **/
object LiveDataViewModel : ViewModel() {

    var position: MutableLiveData<Int>? = MutableLiveData()

    fun setPosition(int: Int) {
        position?.value = int
    }

    fun getPosition(): Int? = position?.value
}