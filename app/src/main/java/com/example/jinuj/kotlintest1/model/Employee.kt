@file:Suppress("unused")

package com.example.jinuj.kotlintest1.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by jinu.j on 25-05-2017.
 **/

open class Employee : Parcelable {


    @SerializedName("employees")
    lateinit var employees: List<User>

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeTypedList(this.employees)
    }

    constructor()

    protected constructor(`in`: Parcel) {
        this.employees = `in`.createTypedArrayList(User.CREATOR)
    }

    companion object {

        val CREATOR: Parcelable.Creator<Employee> = object : Parcelable.Creator<Employee> {
            override fun createFromParcel(source: Parcel): Employee {
                return Employee(source)
            }

            override fun newArray(size: Int): Array<Employee?> {
                return arrayOfNulls(size)
            }
        }
    }

}
