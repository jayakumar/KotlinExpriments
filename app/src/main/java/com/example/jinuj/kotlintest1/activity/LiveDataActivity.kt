package com.example.jinuj.kotlintest1.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.jinuj.kotlintest1.R

class LiveDataActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_data)
    }
}
