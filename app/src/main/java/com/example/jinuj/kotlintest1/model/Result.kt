@file:Suppress("unused")

package com.example.jinuj.kotlintest1.model

import android.os.Parcel
import android.os.Parcelable

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class Result : Parcelable {

    @SerializedName("categoryList")
    @Expose
    var categoryList: List<CategoryList>? = null

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeTypedList(this.categoryList)
    }

    constructor()

    protected constructor(`in`: Parcel) {
        this.categoryList = `in`.createTypedArrayList(CategoryList.CREATOR)
    }

    companion object {

        val CREATOR: Parcelable.Creator<Result> = object : Parcelable.Creator<Result> {
            override fun createFromParcel(source: Parcel): Result {
                return Result(source)
            }

            override fun newArray(size: Int): Array<Result?> {
                return arrayOfNulls(size)
            }
        }
    }
}
