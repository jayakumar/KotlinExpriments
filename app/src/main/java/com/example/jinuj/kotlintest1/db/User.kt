package com.example.jinuj.kotlintest1.db

/**
 * Created by jinu on 04/06/2017.
* */

interface User {

    fun getId(): Int?

    fun getName(): String?

    fun getEmail(): String?

    fun getPhNo(): String?

    fun getAddress(): String?
}
