package com.example.jinuj.kotlintest1.model

import android.os.Parcel
import android.os.Parcelable

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Suppress("unused")
open class CategoryList : Parcelable {

    @SerializedName("categoryid")
    @Expose
    var categoryid: Int? = null


    @SerializedName("categoryname")
    @Expose
    lateinit var categoryname: String


    @SerializedName("parentid")
    @Expose
    lateinit var parentid: String


    @SerializedName("description")
    @Expose
    lateinit var description: String

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeValue(this.categoryid)
        dest.writeString(this.categoryname)
        dest.writeString(this.parentid)
        dest.writeString(this.description)
    }

    constructor()

    protected constructor(`in`: Parcel) {
        this.categoryid = `in`.readValue(Int::class.java.classLoader) as Int
        this.categoryname = `in`.readString()
        this.parentid = `in`.readString()
        this.description = `in`.readString()
    }

    companion object {

        val CREATOR: Parcelable.Creator<CategoryList> = object : Parcelable.Creator<CategoryList> {
            override fun createFromParcel(source: Parcel): CategoryList {
                return CategoryList(source)
            }

            override fun newArray(size: Int): Array<CategoryList?> {
                return arrayOfNulls(size)
            }
        }
    }
}
