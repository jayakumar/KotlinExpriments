package com.example.jinuj.kotlintest1.location

import android.annotation.SuppressLint
import android.arch.lifecycle.*
import android.content.Context
import android.location.Location
import android.os.Bundle
import android.util.Log
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices

@Suppress("unused")
/**
 * Created by jinu.j on 29-05-2017.
 **/
class LocationFetcher : LocationListener,
        LifecycleObserver,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    constructor()

    constructor(lifecycle: Lifecycle) {
        this.lifecycle = lifecycle
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 10000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

    }


    var liveLocation: MutableLiveData<Location>? = null

    private val TAG = "LocationFetcher"
    private var lifecycle: Lifecycle? = null
    private var mApiClient: GoogleApiClient? = null
    private lateinit var mLocationRequest: LocationRequest
    private var isEnabled = false
    private var locationReceiver: LocationReceiver? = null


    companion object {
        var mLocationFetcher: LocationFetcher? = null
        fun getInstance(lifecycle: Lifecycle): LocationFetcher? {
            if (mLocationFetcher == null) {
                mLocationFetcher = LocationFetcher(lifecycle)
            }
            return mLocationFetcher
        }


    }


    fun enableApi(context: Context, locationReceiver: LocationReceiver) {
        isEnabled = true
        this.locationReceiver = locationReceiver
        if (mApiClient == null) {
            mApiClient = GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build()
        }
        if (lifecycle!!.currentState.isAtLeast(Lifecycle.State.STARTED)) {
            mApiClient!!.connect()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        Log.d(TAG, "onResume")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        if (!isEnabled) {
            return
        }
        mApiClient!!.disconnect()
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        if (!isEnabled) {
            return
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        if (!isEnabled) {
            return
        }
        mApiClient!!.connect()
    }


    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    override fun onLocationChanged(location: Location?) {
        locationReceiver?.onGetLocation(location)
    }


    @SuppressLint("MissingPermission")
    override fun onConnected(bundle: Bundle?) {
        LocationServices.FusedLocationApi.requestLocationUpdates(mApiClient,
                mLocationRequest, this)
    }

    override fun onConnectionSuspended(p0: Int) {

    }


}