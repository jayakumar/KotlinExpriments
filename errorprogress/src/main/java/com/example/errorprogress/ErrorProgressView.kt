package com.example.errorprogress

import android.content.Context

import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.DialogTitle
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView

/**
 * Created by jinu.j on 24-05-2017.
 **/
class ErrorProgressView : RelativeLayout {

    lateinit private var mHeader: TextView
    lateinit private var mSubHeader: TextView
    lateinit private var mProgress: ProgressBar
    lateinit private var mErrImage: AppCompatImageView
    lateinit private var mErrButton: Button
    lateinit private var mErrorContainer: View
    lateinit var mOnRetryListener: OnRetryListener


    interface OnRetryListener {
        fun onRetry()
    }


    constructor(context: Context, attr: AttributeSet) : super(context, attr) {
        initView(attr)
    }

    constructor(context: Context, attr: AttributeSet, def: Int) : super(context, attr, def) {
        initView(attr)
    }


    private fun initView(attr: AttributeSet) {
        LayoutInflater.from(context).inflate(R.layout.error_progress_view, this, true)
        mHeader = findViewById(R.id.epv_error_heading)
        mSubHeader = findViewById(R.id.epv_error_sub_heading)
        mProgress = findViewById(R.id.epv_error_progress)
        mErrImage = findViewById(R.id.epv_error_image)
        mErrButton = findViewById(R.id.epv_error_retry_btn)
        mErrorContainer = findViewById(R.id.epv_error_container)
        applyAttr(attr)
        mErrButton.setOnClickListener {
            mOnRetryListener.onRetry()
        }
    }

    private fun applyAttr(attr: AttributeSet) {
        val a = context.obtainStyledAttributes(attr, R.styleable.ErrorProgressView)
        val n = a.length()
        repeat(n) { i ->
            val att = a.getIndex(i)
            when (att) {
                R.styleable.ErrorProgressView_type -> {
                    val x = a!!.getInt(att, 0)
                    if (x == 0) showProgress() else showError()
                }
            }

        }
        a.recycle()
    }

    fun setTitle(title: String) {
        mHeader.text = title
    }

    fun setTitle(title: Int) {
        mHeader.setText(title)
    }

    fun setSubTitle(title: String) {
        mSubHeader.text = title
    }

    fun setSubTitle(title: Int) {
        mSubHeader.setText(title)
    }


    fun showProgress() {
        mErrorContainer.visibility = View.GONE
        mProgress.visibility = View.VISIBLE
    }

    fun showError() {
        mErrorContainer.visibility = View.VISIBLE
        mProgress.visibility = GONE
    }

    fun hideView() {
        this.visibility = View.GONE
    }

    fun showView() {
        this.visibility = View.VISIBLE
    }


}